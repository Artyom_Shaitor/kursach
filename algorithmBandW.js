window.onload = function(){
    
    var canvas = new Canvas("canvas");
    canvas_transp = new Canvas("canvas_transp");
    canvas_transp.R = 182;
    canvas_transp.G = 182;
    canvas_transp.B = 182;
    canvas.canvas.onclick = function(e){Click(e, canvas)};

    canvas.canvas.onmousemove = function(e){
        moveBlock(e);
        if(canvas.click_numb != 0){
            console.log(true);
            canvas_transp.ctx.clearRect(0, 0, 800, 500);
            canvas_transp.current_function(canvas.x0, canvas.y0, e.offsetX, e.offsetY)
        }
    }

    $("input.tool").click(function(){
        $("input.tool").removeClass("active");
        $(this).addClass("active");
        canvas.current_function = canvas.functions[$(this).attr("name")];
        canvas_transp.current_function = canvas_transp.functions[$(this).attr("name")];
    })

    $("#colorpicker").ColorPicker({
        flat: true,
        color: "#000000",
        onChange: function(hsb, hex, rgb){
            canvas.R = rgb['r'];
            canvas.G = rgb['g'];
            canvas.B = rgb['b'];
        }
    });
    $("#reset").click(function(){
        canvas.ctx.clearRect(0, 0, 800, 500);
    });
    
    document.onmouseover = function(){
        $(".position").addClass("hide");
    };
    
}

// moveBlock - функция, перемещающая блок, содержащий координаты курсора относительно холста
function moveBlock(e){
    if((e.offsetX > 0 && e.offsetY > 0) && (e.offsetX < 800 && e.offsetY < 500)){
        $(".position").removeClass("hide");
        $(".position x").text(e.offsetX);
        $(".position y").text(e.offsetY);
        $(".position").css({marginLeft:e.offsetX+20+"px", marginTop:e.offsetY+50+"px"})
    }else {
        $(".position").addClass("hide");
    }
    
}

function Canvas (HTMLObject){
    this.canvas = document.getElementById(HTMLObject);
    this.w = canvas.width;
    this.h = canvas.height;
    this.click_numb = 0;
    this.x0 = 0;
    this.y0 = 0;
    this.x1 = 0;
    this.y1 = 0;
    this.ctx = this.canvas.getContext("2d");
    this.R = 0;
    this.G = 0;
    this.B = 0;
    this.Alpha = 1;
    this.ctx.fillStyle = "rgba("+this.R+","+this.G+","+this.B+","+this.Alpha+")";
    this.canvas.onclick = function(e){Click(e)}
    
    this.PutPixel = function(x, y){
        this.ctx.fillRect(x, y, 1, 1);
    }
    
    //PutPixel - функция, рисующая точку

    this.PutPixel = function(x, y, alpha){
        if(typeof(alpha) == 'undefined') alpha = 1;
        this.ctx.fillStyle = "rgba("+this.R+","+this.G+","+this.B+","+alpha+")";
        this.ctx.fillRect(x, y, 1, 1);
    }

    //DrawPoint - функция рисования пикселя, схожая с PutPixel, только в зависимости от параметра steep меняет местами координаты
    this.DrawPoint = function(steep, x, y, c){
        if(steep){
            tmp = x;
            x = y;
            y = tmp;
        }

        this.PutPixel(x, y, c);
    }

    //BrLine - функиця, рисующая линия по алгоритму Брезенхема
    this.BrLine = function(x0, y0, x1, y1){
        var deltaX = Math.abs(x1 - x0);
        var deltaY = Math.abs(y1 - y0);
        var signX = x0 < x1 ? 1 : -1;
        var signY = y0 < y1 ? 1 : -1;
        
        error = deltaX - deltaY;
        
        this.PutPixel(x1, y1);
        while(x0 != x1 || y0 != y1) {
            this.PutPixel(x0, y0);
            error2 = error * 2;
            //
            if(error2 > -deltaY) {
                error -= deltaY;
                x0 += signX;
            }
            if(error2 < deltaX) {
                error += deltaX;
                y0 += signY;
            }
        }
    }

    //WuLine - функиця, рисующая линия по алгоритму Ву
    this.WuLine = function(x0, y0, x1, y1){
        var steep = Math.abs(y1-y0) > Math.abs(x1 - x0);
        if(steep){
            tmp = x0;
            x0 = y0;
            y0 = tmp;

            tmp = x1;
            x1 = y1;
            y1 = tmp;
        }

        if(x0 > x1){
            tmp = x0;
            x0 = x1;
            x1 = tmp;

            tmp = y0;
            y0 = y1;
            y1 = tmp;
        }


        this.DrawPoint(steep, x0, y0, 1);
        this.DrawPoint(steep, x1, y1, 1);

        var dx = x1 - x0;
        var dy = y1 - y0;
        var gradient = dy/dx;
        var y = y0 + gradient;
        for(var x = x0 + 1; x <= x1 - 1; x++){
            this.DrawPoint(steep, x, ipart(y), 1 - fpart(y));
            this.DrawPoint(steep, x, ipart(y) + 1, fpart(y));
            y += gradient;
        }
    }

    // Circle - функция, рисующая окружность
    this.Circle = function(x0, y0, x1, y1){
        var radius = Math.sqrt(Math.pow(Math.abs(x0 - x1), 2) + Math.pow(Math.abs(y0 - y1), 2));
        var x = 0;
        var y = radius;
        var delta = 1 - 2 * radius;
        var error = 0;
        while(y >= 0){
            this.PutPixel(x0 + x, y0 + y);
            this.PutPixel(x0 + x, y0 - y);
            this.PutPixel(x0 - x, y0 + y);
            this.PutPixel(x0 - x, y0 - y);
            error = 2 * (delta + y) - 1;
            if(delta < 0 && error <= 0) {
                ++x;
                delta += 2 * x + 1;
                continue;
            }
            error = 2 * (delta - x) - 1;
            if(delta > 0 && error > 0) {
                --y;
                delta += 1 - 2 * y;
                continue;
            }
            ++x;
            delta += 2 * (x - y);
            --y;
        }
    }
    this.functions = {
        wu : function(x0, y0, x1, y1){this.WuLine(x0, y0, x1, y1)},
        br : function(x0, y0, x1, y1){this.BrLine(x0, y0, x1, y1)},
        circle : function(x0, y0, x1, y1){this.Circle(x0, y0, x1, y1)}
    }
    this.current_function = this.functions["br"];

}

// Click - функция, выполянемая при клике на холсте
function Click(e, c){
    switch(c.click_numb){
        case 0: c.x0 = e.offsetX; c.y0 = e.offsetY; c.click_numb++; break;
        case 1: 
            c.x1 = e.offsetX; 
            c.y1 = e.offsetY; 
            c.current_function(c.x0, c.y0, c.x1, c.y1); 
            c.click_numb = 0;
            canvas_transp.ctx.clearRect(0, 0, 800, 500); 
        break;
    }
}    

// fpart - дробная часть числа
function fpart(x){
    return x - Math.floor(x);
}

// ipart - целая часть числа
function ipart(x){
    return Math.floor(x);
}
